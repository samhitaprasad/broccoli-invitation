
## Quick start

1.  Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
2.  Clone this repo using `git clone https://samhitaprasad@bitbucket.org/samhitaprasad/broccoli-invitation.git`
3.  Move to the appropriate directory: `cd broccoli-invitation`.
4.  Run `npm install` in order to build and install dependencies.
5.  At this point you can run `npm start` to see the app at `http://localhost:3000`._
6.  Run test cases `npm test`

## Known issues:

* UI Defect:
> Havent handled form data clearing after clicking on send button.

* Some test case failure
```
Test Suites: 4 failed, 47 passed, 51 total
Tests:       9 failed, 194 passed, 203 total
```

* Coverage
```
---------------------------------|----------|----------|----------|----------|-------------------|
File                             |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
---------------------------------|----------|----------|----------|----------|-------------------|
All files                        |    92.31 |    86.41 |    88.68 |    92.11 |                   |
---------------------------------|----------|----------|----------|----------|-------------------|
```
Now you're ready to rumble!

