import styled from 'styled-components';

const Wrapper = styled.footer`
  display: block;
  position: absolute;
  justify-content: space-between;
  text-align: center;
  border-top: 1px solid #666;
  bottom: 0;
  width: 100%;
`;

export default Wrapper;
