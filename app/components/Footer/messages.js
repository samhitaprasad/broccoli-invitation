/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Footer';

export default defineMessages({
  licenseMessage: {
    id: `${scope}.reserved.message`,
    defaultMessage: 'Made with ❤️ in Melbourne.',
  },
  authorMessage: {
    id: `${scope}.message`,
    defaultMessage: `©2019 Broccoli & Co. All Rights Reserved.

    `,
  },
});
