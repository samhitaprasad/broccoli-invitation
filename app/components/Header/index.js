import React from 'react';

import H1 from 'components/H1';
import A from './A';

function Header() {
  return (
    <div>
      <A>
        <H1>Broccoli & Co.</H1>
      </A>
    </div>
  );
}

export default Header;
