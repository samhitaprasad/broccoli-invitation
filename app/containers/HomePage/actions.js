/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  CHANGE_EMAIL,
  CHANGE_CONFIRM_EMAIL,
  OPEN_MODAL,
  CLOSE_MODAL,
  OPEN_SUB_MODAL,
  HIDE_SUB_MODAL,
  UI_ERROR,
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

export function changeEmail(email) {
  return {
    type: CHANGE_EMAIL,
    email,
  };
}

export function changeConfirmEmail(confirmEmail) {
  return {
    type: CHANGE_CONFIRM_EMAIL,
    confirmEmail,
  };
}

export function openModal() {
  return {
    type: OPEN_MODAL,
  };
}

export function closeModal() {
  return {
    type: CLOSE_MODAL,
  };
}

export function openSubModal() {
  return {
    type: OPEN_SUB_MODAL,
  };
}

export function hideSubModal() {
  return {
    type: HIDE_SUB_MODAL,
  };
}

export function setUiError() {
  return {
    type: UI_ERROR,
  };
}
