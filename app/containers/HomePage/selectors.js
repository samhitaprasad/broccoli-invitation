/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );

const makeSelectEmail = () =>
  createSelector(
    selectHome,
    homeState => homeState.email,
  );

const makeSelectConfirmEmail = () =>
  createSelector(
    selectHome,
    homeState => homeState.confirmEmail,
  );

const makeSelectModal = () =>
  createSelector(
    selectHome,
    homeState => homeState.isModalOpen,
  );

const makeSelectSubModal = () =>
  createSelector(
    selectHome,
    homeState => homeState.isSubModalOpen,
  );

const makeSelectUiError = () =>
  createSelector(
    selectHome,
    homeState => homeState.uiError,
  );

export {
  selectHome,
  makeSelectUsername,
  makeSelectEmail,
  makeSelectConfirmEmail,
  makeSelectModal,
  makeSelectSubModal,
  makeSelectUiError,
};
