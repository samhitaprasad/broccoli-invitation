import {
  selectHome,
  makeSelectUsername,
  makeSelectEmail,
  makeSelectConfirmEmail,
} from '../selectors';

describe('selectHome', () => {
  it('should select the home state', () => {
    const homeState = {
      userData: {},
    };
    const mockedState = {
      home: homeState,
    };
    expect(selectHome(mockedState)).toEqual(homeState);
  });
});

describe('makeSelectUsername', () => {
  const usernameSelector = makeSelectUsername();
  it('should select the username', () => {
    const username = 'mxstbr';
    const mockedState = {
      home: {
        username,
      },
    };
    expect(usernameSelector(mockedState)).toEqual(username);
  });
});

describe('makeSelectEmail', () => {
  const emailSelector = makeSelectEmail();
  it('should select the email', () => {
    const email = 'email@email.com';
    const mockedState = {
      home: {
        email,
      },
    };
    expect(emailSelector(mockedState)).toEqual(email);
  });
});

describe('makeSelectConfirmEmail', () => {
  const confirmEmailSelector = makeSelectConfirmEmail();
  it('should select the email', () => {
    const confirmEmail = 'email@email.com';
    const mockedState = {
      home: {
        confirmEmail,
      },
    };
    expect(confirmEmailSelector(mockedState)).toEqual(confirmEmail);
  });
});
