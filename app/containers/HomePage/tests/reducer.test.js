import produce from 'immer';

import homeReducer from '../reducer';
import {
  changeUsername,
  changeEmail,
  changeConfirmEmail,
  openModal,
  closeModal,
  openSubModal,
  hideSubModal,
  setUiError,
} from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('homeReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      username: '',
      email: '',
      confirmEmail: '',
      isModalOpen: false,
      isSubModalOpen: false,
      uiError: false,
    };
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(homeReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the changeUsername action correctly', () => {
    const fixture = 'mxstbr';
    const expectedResult = produce(state, draft => {
      draft.username = fixture;
    });

    expect(homeReducer(state, changeUsername(fixture))).toEqual(expectedResult);
  });

  it('should handle the changeEmail action correctly', () => {
    const fixture = 'usedemail@airwallex.com';
    const expectedResult = produce(state, draft => {
      draft.email = fixture;
    });

    expect(homeReducer(state, changeEmail(fixture))).toEqual(expectedResult);
  });

  it('should handle the changeConfirmEmail action correctly', () => {
    const fixture = 'usedemail@airwallex.com';
    const expectedResult = produce(state, draft => {
      draft.confirmEmail = fixture;
    });

    expect(homeReducer(state, changeConfirmEmail(fixture))).toEqual(
      expectedResult,
    );
  });

  it('should handle the openModal action correctly', () => {
    const fixture = true;
    const expectedResult = produce(state, draft => {
      draft.isModalOpen = fixture;
    });

    expect(homeReducer(state, openModal(fixture))).toEqual(expectedResult);
  });

  it('should handle the closeModal action correctly', () => {
    const fixture = false;
    const expectedResult = produce(state, draft => {
      draft.isModalOpen = fixture;
    });

    expect(homeReducer(state, closeModal(fixture))).toEqual(expectedResult);
  });

  it('should handle the openSubModal action correctly', () => {
    const fixture = true;
    const expectedResult = produce(state, draft => {
      draft.isSubModalOpen = fixture;
    });

    expect(homeReducer(state, openSubModal(fixture))).toEqual(expectedResult);
  });

  it('should handle the hideSubModal action correctly', () => {
    const fixture = false;
    const expectedResult = produce(state, draft => {
      draft.isSubModalOpen = fixture;
    });

    expect(homeReducer(state, hideSubModal(fixture))).toEqual(expectedResult);
  });

  it('should handle the setUiError action correctly', () => {
    const fixture = true;
    const expectedResult = produce(state, draft => {
      draft.uiError = fixture;
    });

    expect(homeReducer(state, setUiError(fixture))).toEqual(expectedResult);
  });
});
