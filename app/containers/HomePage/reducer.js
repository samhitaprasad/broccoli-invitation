/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  CHANGE_USERNAME,
  CHANGE_EMAIL,
  CHANGE_CONFIRM_EMAIL,
  OPEN_MODAL,
  CLOSE_MODAL,
  OPEN_SUB_MODAL,
  HIDE_SUB_MODAL,
  UI_ERROR,
} from './constants';
import { LOAD_REPOS, LOAD_REPOS_SUCCESS } from '../App/constants';
// The initial state of the App
export const initialState = {
  username: '',
  email: '',
  confirmEmail: '',
  isModalOpen: false,
  isSubModalOpen: false,
  uiError: false,
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_USERNAME:
        draft.username = action.username;
        break;
      case CHANGE_EMAIL:
        draft.email = action.email;
        break;
      case CHANGE_CONFIRM_EMAIL:
        draft.confirmEmail = action.confirmEmail;
        break;
      case OPEN_MODAL:
        draft.isModalOpen = true;
        break;
      case CLOSE_MODAL:
        draft.isModalOpen = false;
        break;
      case OPEN_SUB_MODAL:
        draft.isSubModalOpen = true;
        break;
      case HIDE_SUB_MODAL:
        draft.isSubModalOpen = false;
        break;
      case UI_ERROR:
        draft.uiError = true;
        break;
      case LOAD_REPOS:
        draft.uiError = false;
        break;
      case LOAD_REPOS_SUCCESS:
        draft.isSubModalOpen = true;
        draft.isModalOpen = false;
        break;
    }
  });

export default homeReducer;
