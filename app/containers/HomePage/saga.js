/**
 * Gets the data from the end-point
 */

import { put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_REPOS } from 'containers/App/constants';
import { reposLoaded, repoLoadingError } from 'containers/App/actions';

import axios from 'axios';
import {
  makeSelectUsername,
  makeSelectEmail,
} from 'containers/HomePage/selectors';

/**
 * APIs request/response handler
 */
export function* getRepos() {
  // Select name and email from store
  const name = yield select(makeSelectUsername());
  const email = yield select(makeSelectEmail());
  const requestURL = `https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth`;
  const response = yield axios // eslint-disable-next-line no-shadow
    .post(requestURL, { name, email }) // eslint-disable-next-line no-shadow
    .then(response => response) // eslint-disable-next-line no-shadow
    .catch(error => error.response);

  if (response.status === 200) {
    yield put(reposLoaded(response));
  } else {
    yield put(repoLoadingError(response));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_REPOS, getRepos);
}
