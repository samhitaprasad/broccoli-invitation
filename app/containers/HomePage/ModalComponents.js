/**
 *
 * ModalComponent.js
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import ModalTitle from 'react-bootstrap/ModalTitle';

import Form from 'react-bootstrap/Form';
import H3 from 'components/H3';
import Button from 'react-bootstrap/Button';

function ModalComponent(props) {
  const modalComponent = (
    <div>
      <Modal show={props.modal} onHide={props.onCloseModal}>
        {/* <Modal.Header>
            <ModalTitle>Request on invite</ModalTitle>
          </Modal.Header> */}
        <hr />
        <ModalTitle
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          Request on invite
        </ModalTitle>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          ______
        </div>
        <Modal.Body>
          <Form onSubmit={props.onSubmitForm}>
            <Form.Group>
              <Form.Label>Full name</Form.Label>
              <Form.Control
                required
                id="username"
                minLength="3"
                type="text"
                value={props.username}
                onChange={props.onChangeUsername}
                placeholder="Full name"
              />
              <Form.Label>Email</Form.Label>
              <Form.Control
                required
                id="email"
                type="email"
                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$"
                value={props.email}
                onChange={props.onChangeEmail}
                placeholder="Email"
              />
              <Form.Label>Confirm email</Form.Label>
              <Form.Control
                required
                id="confirmEmail"
                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$"
                type="email"
                value={props.confirmEmail}
                onChange={props.onChangeConfirmEmail}
                placeholder="Confirm email"
              />
            </Form.Group>
            <Button
              style={{ width: '100%' }}
              type="submit"
              disabled={props.loading}
              className="btn btn-primary"
            >
              {props.loading ? 'Sending, please wait...' : 'Send'}
            </Button>
          </Form>
          <br />
          <div
            style={{
              color: 'red',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {props.uiError ? <p>Confirm Email needs to match Email.</p> : null}
            {props.error ? <p>{props.error.errorMessage}</p> : null}
          </div>
          <hr />
        </Modal.Body>
      </Modal>
      <Modal show={props.subModal} onHide={props.onCloseSubModal}>
        <hr />
        <ModalTitle
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          All Done!
        </ModalTitle>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          ______
        </div>
        <Modal.Body>
          <hr />
          <H3>You will be the first to experience</H3>
          <H3>Broccoli & Co. when we launch</H3>
        </Modal.Body>
        <hr />
        <Button
          style={{
            margin: '0 auto',
            variant: 'primary',
            width: '90%',
            display: 'flex',
            align: 'center',
            justifyContent: 'center',
          }}
          className="btn btn-default"
          onClick={props.onCloseSubModal}
        >
          Ok
        </Button>
        <hr />
      </Modal>
    </div>
  );

  return <div>{modalComponent}</div>;
}

ModalComponent.propTypes = {
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  email: PropTypes.string,
  confirmEmail: PropTypes.string,
  onChangeUsername: PropTypes.func,
  onChangeEmail: PropTypes.func,
  onChangeConfirmEmail: PropTypes.func,
  onCloseModal: PropTypes.func,
  onCloseSubModal: PropTypes.func,
  modal: PropTypes.bool,
  subModal: PropTypes.bool,
  uiError: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.object,
};

export default ModalComponent;
