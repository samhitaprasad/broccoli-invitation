/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.containers.HomePage';

export default defineMessages({
  startProjectHeader: {
    id: `${scope}.start.header`,
    defaultMessage: 'A better way to enjoy everyday.',
  },
  startProjectMessage: {
    id: `${scope}.start.message`,
    defaultMessage: 'Be the first to know when we launch.',
  },
});
