/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';
export const CHANGE_EMAIL = 'boilerplate/Home/CHANGE_EMAIL';
export const CHANGE_CONFIRM_EMAIL = 'boilerplate/Home/CHANGE_CONFIRM_EMAIL';
export const OPEN_MODAL = 'boilerplate/Home/OPEN_MODAL';
export const CLOSE_MODAL = 'boilerplate/Home/CLOSE_MODAL';
export const OPEN_SUB_MODAL = 'boilerplate/Home/OPEN_SUB_MODAL';
export const HIDE_SUB_MODAL = 'boilerplate/Home/HIDE_SUB_MODAL';
export const UI_ERROR = 'boilerplate/Home/UI_ERROR';
