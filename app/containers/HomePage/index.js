/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Button from 'react-bootstrap/Button';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import H2 from 'components/H2';
import CenteredSection from './CenteredSection';
import messages from './messages';
import { loadRepos } from '../App/actions';
import {
  changeUsername,
  changeEmail,
  changeConfirmEmail,
  openModal,
  closeModal,
  setUiError,
  hideSubModal,
} from './actions';
import {
  makeSelectUsername,
  makeSelectEmail,
  makeSelectConfirmEmail,
  makeSelectModal,
  makeSelectSubModal,
  makeSelectUiError,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import ModalComponent from './ModalComponents';

const key = 'home';

export function HomePage({
  username,
  email,
  confirmEmail,
  loading,
  error,
  onSubmitForm,
  onChangeUsername,
  onChangeEmail,
  onChangeConfirmEmail,
  onOpenModal,
  onCloseModal,
  onCloseSubModal,
  modal,
  subModal,
  uiError,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  return (
    <article>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="broccoli"
          content="A React.js Boilerplate application for broccoli"
        />
      </Helmet>
      <div>
        <CenteredSection>
          <H2>
            <FormattedMessage {...messages.startProjectHeader} />
          </H2>
          <p>
            <FormattedMessage {...messages.startProjectMessage} />
          </p>
          <Button onClick={onOpenModal}>Request an invite</Button>
        </CenteredSection>
        <ModalComponent
          modal={modal}
          onCloseModal={onCloseModal}
          onSubmitForm={onSubmitForm}
          username={username}
          onChangeUsername={onChangeUsername}
          email={email}
          onChangeEmail={onChangeEmail}
          confirmEmail={confirmEmail}
          onChangeConfirmEmail={onChangeConfirmEmail}
          loading={loading}
          uiError={uiError}
          error={error}
          subModal={subModal}
          onCloseSubModal={onCloseSubModal}
        />
      </div>
    </article>
  );
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  email: PropTypes.string,
  confirmEmail: PropTypes.string,
  onChangeUsername: PropTypes.func,
  onChangeEmail: PropTypes.func,
  onChangeConfirmEmail: PropTypes.func,
  onOpenModal: PropTypes.func,
  onCloseModal: PropTypes.func,
  onUiError: PropTypes.func,
  onCloseSubModal: PropTypes.func,
  modal: PropTypes.bool,
  subModal: PropTypes.bool,
  uiError: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  email: makeSelectEmail(),
  confirmEmail: makeSelectConfirmEmail(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  modal: makeSelectModal(),
  subModal: makeSelectSubModal(),
  uiError: makeSelectUiError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onChangeEmail: evt => dispatch(changeEmail(evt.target.value)),
    onChangeConfirmEmail: evt => dispatch(changeConfirmEmail(evt.target.value)),
    onOpenModal: () => dispatch(openModal()),
    onCloseModal: () => dispatch(closeModal()),
    onCloseSubModal: () => dispatch(hideSubModal()),
    onUiError: () => dispatch(setUiError()),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      if (evt.target.email.value === evt.target.confirmEmail.value) {
        dispatch(loadRepos());
      } else {
        evt.preventDefault();
        dispatch(setUiError());
      }
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
