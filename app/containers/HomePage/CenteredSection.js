import styled from 'styled-components';

const CenteredSection = styled.section`
  margin: 5em auto;
  padding: 3em;
  text-align: center;
`;

export default CenteredSection;
