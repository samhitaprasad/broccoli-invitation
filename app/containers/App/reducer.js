/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { LOAD_REPOS_SUCCESS, LOAD_REPOS, LOAD_REPOS_ERROR } from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: {
    errorMessage: false,
    status: false,
    statusText: false,
  },
  currentUser: false,
  userData: {
    data: false,
    status: false,
    statusText: false,
  },
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_REPOS:
        draft.loading = true;
        draft.error.errorMessage = false;
        draft.error.status = false;
        draft.error.statusText = false;
        draft.userData.data = false;
        draft.userData.status = false;
        draft.userData.statusText = false;
        break;

      case LOAD_REPOS_SUCCESS:
        draft.userData.data = action.response.data;
        draft.userData.status = action.response.status;
        draft.userData.statusText = action.response.statusText;
        draft.loading = false;
        draft.currentUser = action.username;
        break;

      case LOAD_REPOS_ERROR:
        draft.error.errorMessage = action.error.data.errorMessage;
        draft.error.status = action.error.status;
        draft.error.statusText = action.error.statusText;
        draft.loading = false;
        break;
    }
  });

export default appReducer;
