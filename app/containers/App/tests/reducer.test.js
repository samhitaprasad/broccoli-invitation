import produce from 'immer';

import appReducer from '../reducer';
import { loadRepos, reposLoaded, repoLoadingError } from '../actions';

/* eslint-disable default-case, no-param-reassign */
describe('appReducer', () => {
  let state;
  beforeEach(() => {
    state = {
      loading: false,
      error: {
        errorMessage: false,
        status: false,
        statusText: false,
      },
      currentUser: false,
      userData: {
        data: false,
        status: false,
        statusText: false,
      },
    };
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(appReducer(undefined, {})).toEqual(expectedResult);
  });

  it('should handle the loadRepos action correctly', () => {
    const expectedResult = produce(state, draft => {
      draft.loading = true;
      draft.error.errorMessage = false;
      draft.error.status = false;
      draft.error.statusText = false;
      draft.userData.data = false;
      draft.userData.status = false;
      draft.userData.statusText = false;
    });

    expect(appReducer(state, loadRepos())).toEqual(expectedResult);
  });

  it('should handle the reposLoaded action correctly', () => {
    const fixture = {
      data: 'Registered',
      status: 200,
      statusText: 'OK',
    };
    const username = 'test';
    const expectedResult = produce(state, draft => {
      draft.userData.data = 'Registered';
      draft.userData.status = 200;
      draft.userData.statusText = 'OK';
      draft.loading = false;
      draft.currentUser = username;
    });

    expect(appReducer(state, reposLoaded(fixture, username))).toEqual(
      expectedResult,
    );
  });

  it('should handle the repoLoadingError action correctly', () => {
    const fixture = {
      data: { errorMessage: 'Bad Request: Email is already in use' },
      status: 400,
      statusText: 'Bad Request',
    };
    const expectedResult = produce(state, draft => {
      draft.error.errorMessage = 'Bad Request: Email is already in use';
      draft.error.status = 400;
      draft.error.statusText = 'Bad Request';
      draft.loading = false;
    });

    expect(appReducer(state, repoLoadingError(fixture))).toEqual(
      expectedResult,
    );
  });
});
